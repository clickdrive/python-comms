from cdcomms import ClickdriveConnection
from cdcomms import ClickdriveConstants
from cdcomms import Subscription

import time


def sub_cbk(subscription, value):
    print "[%s]:%s" % (subscription.get_info_name(), value)


#with ClickdriveConnection("192.168.2.242", 10000, "192.168.2.115") as cnt:
with ClickdriveConnection("192.168.8.1", 1000, "192.168.8.69") as cnt:
    print "connected"
    c1 = cnt.subscribe(ClickdriveConstants.INF_IMU, "IMU.FUSION_QPOSE", sub_cbk)
    c2 = cnt.subscribe(ClickdriveConstants.INF_IMU, "IMU.GYRO", sub_cbk)
    time.sleep(5)
    cnt.unsubscribe(c1)
    cnt.unsubscribe(c1)
    cnt.unsubscribe(c1)
    time.sleep(1500)
