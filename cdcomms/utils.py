class Enum(dict):
    def __init__(self, arr):
        super(Enum, self).__init__({k: v for v, k in enumerate(arr)})
        self._reverse_map = {k: v for v, k in self.items()}

    def __getattr__(self, name):
        if name in self:
            return super(Enum, self).__getitem__(name)
        raise AttributeError

    def __getitem__(self, item):
        return self._reverse_map[item]


def to_hex(data):
    return ":".join("{:02x}".format(ord(c)) for c in data)
