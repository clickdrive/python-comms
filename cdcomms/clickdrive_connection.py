import socket
import zmq

from messages import *
from serializer import MessagesSerializer
from serializer import Subscription
from threading import Thread
from threading import RLock
import random
import time

class ClickdriveConstants:
    def __init__(self):
        pass

    INF_INFORMATION = ClientRequestSubscribe.RFI.IMU
    INF_EXCEPTION = ClientRequestSubscribe.RFI.EXCEPTION
    INF_OBDII = ClientRequestSubscribe.RFI.OBDII
    INF_EVENT = ClientRequestSubscribe.RFI.EVENT
    INF_IMU = ClientRequestSubscribe.RFI.IMU
    INF_GPS = ClientRequestSubscribe.RFI.GPS


class ClickdriveTimeoutException(Exception):
    pass


class ClickdriveInvalidState(Exception):
    pass


class ClickdriveConnection:
    __CDRIVE_REQUEST_PORT = 5554
    __CDRIVE_SUBSCRIPTION_PORT = 5555
    __CDRIVE_HEARTBEAT_INTERVAL_SECONDS = 5
    __ZMQ_CTX_REQUEST = zmq.Context()
    __ZMQ_DEFAULT_TIMEOUT = 5000

    def _ctx(self):
        return self.__class__.__ZMQ_CTX_REQUEST

    @staticmethod
    def _get_io_timeout():
        return 0

    def _subscription_thread_func(self):
        while True:
            # print "SUB"
            if self._subscription_running:
                if self._socket_subscription.poll(1000, zmq.POLLIN):
                    data = self._socket_subscription.recv()
                    MessagesSerializer.deserialize_subscription_stream(self._subscriptions, data)
            else:
                self._socket_subscription.close()
                return

    def _heartbeat_thread_func(self):
        while True:
            with self._io_lock:
                # print "HBT"
                if self._io_error:
                    return
                if not self._heartbeat_running:
                    return

                for i in xrange(3):
                    try:
                        self._send(ClientRequestHeartBeat(self._client_id), 5000)
                        break
                    except ClickdriveTimeoutException:
                        self._io_error = True
                        return

            time.sleep(self.__class__.__CDRIVE_HEARTBEAT_INTERVAL_SECONDS)

            with self._io_lock:
                for s in self._subscriptions.values():
                    print ">> %s" % s.get_info_name()
                    self._send(ClientRequestSubscribe(self._client_id, s.get_info_type(), s.get_info_name()))

    def _send(self, msg, timeout=None):
        self._socket_req.send(MessagesSerializer.serialize_client_request(msg), flags=zmq.NOBLOCK)

        if self._socket_req.poll(timeout if timeout else self.__class__.__ZMQ_DEFAULT_TIMEOUT, zmq.POLLIN) == 0:
            raise ClickdriveTimeoutException("Unable to send data, timeout")

        return MessagesSerializer.deserialize_server_reply(self._socket_req.recv())

    def _recv(self, timeout=None):
        if self._socket_reply.poll(timeout if timeout else self.__class__.__ZMQ_DEFAULT_TIMEOUT, zmq.POLLIN) == 0:
            raise ClickdriveTimeoutException("Unable to receive data, timeout")

        msg = MessagesSerializer.deserialize_server_reply(self._socket_reply.recv())
        self._socket_reply.send(MessagesSerializer.serialize_client_reply(Ack.ack()))

        return msg

    def _disconnect(self):
        with self._io_lock:
            if self._client_connected and not self._io_error:
                self._socket_req.send(
                        MessagesSerializer.serialize_client_request(ClientRequestTerminate(self._client_id)))

            self._heartbeat_running = False

        self._subscription_running = False

        if self._socket_req:
            self._socket_req.close()
            self._socket_req = None

        if self._socket_reply:
            self._socket_reply.close()
            self._socket_reply = None

        self._client_connected = False

    def _connect(self):
        self._disconnect()

        self._socket_req = self._ctx().socket(zmq.REQ)
        self._socket_req.setsockopt(zmq.LINGER, 0)

        self._socket_reply = self._ctx().socket(zmq.REP)
        self._socket_reply.setsockopt(zmq.LINGER, 0)

        self._socket_subscription = self._ctx().socket(zmq.SUB)

        reply_socket_port = self._socket_reply.bind_to_random_port("tcp://" + self._local_host_addr)

        self._socket_req.connect("tcp://%s:%d" % (self._clickdrive_host, self.__class__.__CDRIVE_REQUEST_PORT))
        self._send(ClientRequestHandshake(str(random.randint(1, 100000)),
                                          '1.0',
                                          'tcp://%s:%d' % (self._local_host_addr, reply_socket_port)),
                   self._io_timeout)

        self._socket_subscription.connect(
                "tcp://%s:%d" % (self._clickdrive_host, self.__class__.__CDRIVE_SUBSCRIPTION_PORT))
        self._socket_subscription.setsockopt(zmq.SUBSCRIBE, '')

        msg = self._recv(self._io_timeout)
        self._client_id = msg.get_client_id()

        self._heartbeat_running = True
        self._subscription_running = True

        self._heartbeat_thread = Thread(target=self._heartbeat_thread_func)
        self._heartbeat_thread.daemon = True
        self._heartbeat_thread.start()

        self._subscription_thread = Thread(target=self._subscription_thread_func)
        self._subscription_thread.daemon = True
        self._subscription_thread.start()

        self._client_connected = True

    def __init__(self, clickdrive, timeout=None, local_host=None):
        """
        :param clickdrive: ip address of the clickdrive device
        :param timeout: connection timeout
        :param local_host: optional, use it on the multihomed hosts
        :return:
        """
        self._clickdrive_host = clickdrive
        self._local_host_addr = local_host or socket.gethostbyname(socket.gethostname())
        self._socket_req = None
        self._socket_reply = None
        self._client_id = None
        self._io_timeout = timeout

        self._heartbeat_running = False
        self._heartbeat_thread = None
        self._io_lock = RLock()
        self._io_error = False
        self._client_connected = False

        self._subscription_running = False
        self._subscription_thread = None

        self._subscriptions = {}
        self._connect()

    def __nonzero__(self):
        return self._client_connected

    def close(self):
        self._disconnect()

    def __enter__(self):
        if not self._client_connected:
            raise ClickdriveInvalidState("socket not connected")

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._disconnect()

    def request_for_channel_info_by_name(self, channel):
        with self._io_lock:
            try:
                if self._io_error:
                    raise ClickdriveInvalidState("socket in the error state")
                req = ClientRequestForInformation(ClientRequestForInformation.RFI.INFORMATION,
                                                  "INFORMATION.GFIRN " + channel)
                return self._send(req)
            except ClickdriveTimeoutException:
                self._io_error = True
                raise

    def subscribe(self, info_type, info_name, callback):
        """
        :param info_type: ClickdriveConstants INF_ values
        :param info_name: information element name
        :param callback:  callback function to be called with parameters (Subscription, [data array])
        :return: id of the subscription
        """
        with self._io_lock:
            try:
                if self._io_error:
                    raise ClickdriveInvalidState("socket in the error state")

                req = ClientRequestSubscribe(self._client_id, info_type, info_name)
                sub = self._send(req)
                if sub:
                    self._subscriptions[sub.get_subscription_id()] = Subscription(sub, info_type, info_name,
                                                                                  callback)
                    return sub.get_subscription_id()
                else:
                    return None

            except ClickdriveTimeoutException:
                self._io_error = True
                raise

    def unsubscribe(self, subscription_id):
        with self._io_lock:
            if self._subscriptions.pop(subscription_id, None) is not None:
                self._send(ClientRequestTerminate(self._client_id, subscription_id))
