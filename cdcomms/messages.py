from utils import Enum


class MessageToClient(object):
    MESSAGE_STATUS = Enum(['OK', 'ERROR', 'ACTION_REQUIRED'])
    MESSAGE_TYPE = Enum(
            ['ACK', 'ASYNC_ACK', 'ACK_RESPONSE', 'HANDSHAKE', 'INFO_REQUEST', 'SUB_REQUEST', 'STREAM_REFRESH',
             'STREAM', 'SEND_REQUEST', 'UNKNOWN'])

    def __init__(self, response_type, response_status):
        self._response_type = response_type
        self._response_status = response_status

    def get_message_status(self):
        return self._response_status

    def get_message_type(self):
        return self._response_type

    def __nonzero__(self):
        return self._response_status != self.__class__.MESSAGE_STATUS.ERROR

    def __repr__(self):
        return "%s(%r)" % (self.__class__, self.__dict__)

    def __str__(self):
        return "MessageToClient: { type: %s, status: %s }" % (self.__class__.MESSAGE_TYPE[self._response_type],
                                                              self.__class__.MESSAGE_STATUS[self._response_status])


class Ack(MessageToClient):
    STATE = Enum(['ACK', 'NACK'])

    def __init__(self, msg):
        super(Ack, self).__init__(msg.get_message_type(), msg.get_message_status())

    @staticmethod
    def ack():
        return MessageToClient(MessageToClient.MESSAGE_TYPE.ACK, MessageToClient.MESSAGE_STATUS.OK)

    @staticmethod
    def nack():
        return MessageToClient(MessageToClient.MESSAGE_TYPE.ACK, MessageToClient.MESSAGE_STATUS.ERROR)

    def __str__(self):
        return "ACK { %s }" % super(Ack, self).__str__()


class HandshakeResponse(MessageToClient):
    def __init__(self, client_id, msg):
        super(HandshakeResponse, self).__init__(msg.get_message_type(), msg.get_message_status())
        self._client_id = client_id

    def get_client_id(self):
        return self._client_id

    def __str__(self):
        return "HandshakeResponse: { %s client_id: %d }" % (super(HandshakeResponse, self).__str__(), self._client_id)


class ChannelInfoResponse(MessageToClient):
    def __init__(self, c_name, c_id, c_data_type, c_size, msg):
        super(ChannelInfoResponse, self).__init__(msg.get_message_type(), msg.get_message_status())
        self._channel_name = c_name
        self._channel_id = c_id
        self._channel_data_type = c_data_type
        self._channel_size = c_size

    def get_channel_name(self):
        return self._channel_name

    def get_channel_id(self):
        return self._channel_id

    def get_channel_data_type(self):
        return self._channel_data_type

    def get_channel_size(self):
        return self._channel_size

    def __str__(self):
        return "ChannelInfoResponse { %s  name: %s, id: %s, data_type: %s, size: %s }" % \
               (super(ChannelInfoResponse, self).__str__(), self._channel_name,
                self._channel_id, self._channel_data_type, self._channel_size)


class SubscriptionResponse(MessageToClient):
    def __init__(self, subscription_id, data_type, data_size, msg):
        super(SubscriptionResponse, self).__init__(msg.get_message_type(), msg.get_message_status())
        self._subscription_id = subscription_id
        self._data_type = data_type
        self._data_size = data_size

    def get_subscription_id(self):
        return self._subscription_id

    def get_data_type(self):
        return self._data_type

    def get_data_size(self):
        return self._data_size

    def __str__(self):
        return "SubscriptionResponse { %s  id: %d, data_type: %d, size: %d }" % \
               (super(SubscriptionResponse, self).__str__(), self._subscription_id,
                self._data_type, self._data_size)


class ClientRequest(object):
    REQUESTS = Enum(['RFI', 'RFS', 'HEARTBEAT', 'HANDSHAKE', 'TERMINATE', 'REQUEST_CHANNEL', 'OPEN_CHANNEL'])

    def __init__(self, request_type):
        if request_type not in ClientRequest.REQUESTS.values():
            raise ValueError("invalid request type")

        self._request_type = request_type

    def get_request_type(self):
        return self._request_type


class ClientRequestHeartBeat(ClientRequest):
    def __init__(self, client_id):
        super(ClientRequestHeartBeat, self).__init__(self.__class__.REQUESTS.HEARTBEAT)
        self._client_id = client_id

    def get_client_id(self):
        return self._client_id


class ClientRequestHandshake(ClientRequest):
    def __init__(self, guid, version_id, host):
        super(ClientRequestHandshake, self).__init__(self.__class__.REQUESTS.HANDSHAKE)
        self.guid = guid
        self.version_id = version_id
        self.host = host

    def get_guid(self):
        return self.guid

    def get_version_id(self):
        return self.version_id

    def get_host(self):
        return self.host


class ClientRequestForInformation(ClientRequest):
    RFI = Enum(['INFORMATION', 'EXCEPTION', 'OBDII', 'EVENT', 'IMU', 'GPS'])

    def __init__(self, info_type, code_string):
        if info_type not in self.__class__.RFI.values():
            raise ValueError("invalid info_type %d" % info_type)

        super(ClientRequestForInformation, self).__init__(self.__class__.REQUESTS.RFI)
        self._info_type = info_type
        self._code_string = code_string

    def get_information_type(self):
        return self._info_type

    def get_code_string(self):
        return self._code_string


class ClientRequestSubscribe(ClientRequest):
    RFI = Enum(['INFORMATION', 'EXCEPTION', 'OBDII', 'EVENT', 'IMU', 'GPS'])

    def __init__(self, client_id, info_type, info_name):
        if info_type not in self.__class__.RFI.values():
            raise ValueError("invalid info_type %d" % info_type)
        super(ClientRequestSubscribe, self).__init__(self.__class__.REQUESTS.RFS)
        self._client_id = client_id
        self._info_type = info_type
        self._info_name = info_name

    def get_client_id(self):
        return self._client_id

    def get_info_type(self):
        return self._info_type

    def get_info_name(self):
        return self._info_name


class ClientRequestTerminate(ClientRequest):
    def __init__(self, client_id, subscription_id=None):
        super(ClientRequestTerminate, self).__init__(self.__class__.REQUESTS.TERMINATE)
        self._client_id = client_id
        self._subscription_id = subscription_id

    def get_client_id(self):
        return self._client_id

    def get_subscription_id(self):
        return self._subscription_id
