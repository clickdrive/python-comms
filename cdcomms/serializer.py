import msgpack
import StringIO

from cdcomms.utils import to_hex
from messages import *


class Subscription(object):
    def __init__(self, subscription_response, info_type, info_name, callback):
        self._subscription_response = subscription_response
        self._info_type = info_type
        self._info_name = info_name
        self._callback = callback

    def get_info_type(self):
        return self._info_type

    def get_info_name(self):
        return self._info_name

    def get_data_type(self):
        return self._subscription_response.get_data_type()

    def get_data_size(self):
        return self._subscription_response.get_data_size()

    def get_callback(self):
        return self._callback


class MessagesSerializer(object):
    # _DATA_TYPES = Enum(BOOLEAN_ARRAY, BYTE_ARRAY, CHAR_ARRAY, SHORT_ARRAY, INT_ARRAY, LONG_ARRAY,
    #                    FLOAT_ARRAY, DOUBLE_ARRAY, STRING_ARRAY)

    def __init__(self):
        pass

    @staticmethod
    def serialize_client_request(request):
        def pack(data):
            return list(bytes(msgpack.packb(data)))

        out_data = pack(request.get_request_type())

        if request.get_request_type() == ClientRequest.REQUESTS.HEARTBEAT:
            out_data += pack(request.get_client_id())
        elif request.get_request_type() == ClientRequest.REQUESTS.HANDSHAKE:
            out_data += pack(request.get_guid())
            out_data += pack(request.get_version_id())
            out_data += pack(request.get_host())
        elif request.get_request_type() == ClientRequest.REQUESTS.RFI:
            out_data += pack(request.get_information_type())
            out_data += pack(request.get_code_string())
        elif request.get_request_type() == ClientRequest.REQUESTS.RFS:
            out_data += pack(request.get_client_id())
            out_data += pack(request.get_info_type())
            out_data += pack(request.get_info_name())
        elif request.get_request_type() == ClientRequest.REQUESTS.TERMINATE:
            out_data += pack(request.get_client_id())
            if request.get_subscription_id() is not None:
                out_data += pack(int(request.get_subscription_id()))
        else:
            raise ValueError("unable to serialize request, invalid request type")

        return bytearray(out_data)

    @staticmethod
    def serialize_client_reply(reply):
        def pack(data):
            return list(bytes(msgpack.packb(data)))

        out_data = pack(reply.get_message_type())

        if reply.get_message_type() == MessageToClient.MESSAGE_TYPE.ACK:
            out_data += pack(reply.get_message_status())
        else:
            raise ValueError("invalid client reply " + reply)

        return bytearray(out_data)

    @staticmethod
    def deserialize_server_request(req):
        unpacker = msgpack.Unpacker(StringIO.StringIO(req))


    @staticmethod
    def deserialize_server_reply(reply):
        unpacker = msgpack.Unpacker(StringIO.StringIO(reply))

        try:
            msg = MessageToClient(unpacker.next(), unpacker.next())
        except Exception:
            return MessageToClient(MessageToClient.MESSAGE_TYPE.UNKNOWN, MessageToClient.MESSAGE_STATUS.ERROR)

        if msg.get_message_status() == MessageToClient.MESSAGE_STATUS.ERROR:
            return msg

        if (msg.get_message_type() == MessageToClient.MESSAGE_TYPE.ACK or
                    msg.get_message_type() == MessageToClient.MESSAGE_TYPE.ASYNC_ACK):
            return Ack(msg)
        elif msg.get_message_type() == MessageToClient.MESSAGE_TYPE.HANDSHAKE:
            return HandshakeResponse(unpacker.next(), msg)
        elif msg.get_message_type() == MessageToClient.MESSAGE_TYPE.INFO_REQUEST:
            unpacker.next()
            unpacker.next()
            c = unpacker.next()
            return ChannelInfoResponse(c['dataType'], c['channelId'], c['name'], c['size'], msg)
        elif msg.get_message_type() == MessageToClient.MESSAGE_TYPE.SUB_REQUEST:
            return SubscriptionResponse(unpacker.next(), unpacker.next(), unpacker.next(), msg)
        else:
            raise ValueError("invalid reply received: %s" % to_hex(reply))

    @staticmethod
    def deserialize_subscription_stream(subscriptions, data):
        unpacker = msgpack.Unpacker(StringIO.StringIO(data))

        sub = subscriptions.get(unpacker.next())

        if sub:
            val = []
            for i in xrange(sub.get_data_size()):
                val.append(unpacker.next())
            if sub.get_callback is not None:
                sub.get_callback()(sub, val)

        else:
            return None
